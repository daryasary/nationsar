from django.urls import path, re_path

from main.views import HomePage, MissionsList, ContactView, HelpView

urlpatterns = [
    path('', HomePage.as_view(), name='home_page'),
    path('missions', MissionsList.as_view(), name='missions_list'),
    path('contact', ContactView.as_view(), name='contacts'),
    path('help', HelpView.as_view(), name='help'),
]
