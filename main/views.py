from constance import config
from django.contrib.flatpages.models import FlatPage
from django.shortcuts import render
from django.views import View
from django.views.generic import DetailView, TemplateView

from mission.models import Mission, Activity


class HomePage(View):
    """Home is a static skeleton with dynamic contents here we just fetch
        dynamic contents of each piece of defined home page"""
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        missions = Mission.objects.filter(featured=True).order_by('priority')
        manifest = FlatPage.objects.filter(url='/manifest/').first()
        context = dict(view_name='home', missions=missions, manifest=manifest)
        return render(request, self.template_name, context=context)


class MissionsList(View):
    template_name = 'missions.html'

    def get(self, request, *args, **kwargs):
        missions = Mission.objects.all()
        activities = Activity.objects.all()[:9]
        context = dict(view_name='missions', missions=missions, activities=activities, mission_text=config.mission_text)
        return render(request, self.template_name, context=context)


class ContactView(TemplateView):
    template_name = 'contacts.html'
    extra_context = {'view_name': 'contact'}


class HelpView(TemplateView):
    template_name = 'help.html'
    extra_context = {'view_name': 'help'}
