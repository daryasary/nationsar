from django import forms
from django.contrib import admin
from django.contrib.flatpages.forms import FlatpageForm as FlatpageFormOld
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import gettext_lazy as _

from ckeditor.widgets import CKEditorWidget

admin.site.unregister(FlatPage)


class FlatpageForm(FlatpageFormOld):
    widget = CKEditorWidget(config_name='flat')
    content = forms.CharField(widget=widget)
    content_en = forms.CharField(widget=widget)
    content_fa = forms.CharField(widget=widget)

    class Meta:
        model = FlatPage
        fields = '__all__'


@admin.register(FlatPage)
class FlatPageAdmin(admin.ModelAdmin):
    form = FlatpageForm
    fieldsets = (
        (None, {'fields': (
            'url', 'title', 'title_en', 'title_fa', 'content', 'content_en',
            'content_fa', 'sites')}),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': ('registration_required', 'template_name'),
        }),
    )
    list_display = ('url', 'title')
    list_filter = ('sites', 'registration_required')
    search_fields = ('url', 'title')
