from django.db import models

from django.utils.translation import ugettext_lazy as _


class Mission(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=255)
    header_image = models.ImageField(verbose_name=_("Header image"), upload_to="mission/headers")
    feature_image = models.ImageField(verbose_name=_("Feature image"), upload_to="mission/feature")
    body = models.TextField(verbose_name=_("Mission text body"))

    featured = models.BooleanField(verbose_name=_("Is featured"), default=False)
    priority = models.IntegerField(verbose_name=_("Priority"), unique=True)

    created_time = models.DateTimeField(verbose_name=_("Created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("Modified time"), auto_now=True)

    class Meta:
        verbose_name = _("Mission")
        verbose_name_plural = _("Missions")
        ordering = ['priority']

    def __str__(self):
        return self.title


class Activity(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=255)
    header_image = models.ImageField(verbose_name=_("Header image"), upload_to="activity/headers")
    feature_image = models.ImageField(verbose_name=_("Feature image"), upload_to="activity/feature")
    body = models.TextField(verbose_name=_("Mission text body"))

    featured = models.BooleanField(verbose_name=_("Is featured"), default=False)
    priority = models.IntegerField(verbose_name=_("Priority"))

    mission = models.ForeignKey(Mission, related_name="activities", on_delete=models.SET_NULL, null=True)

    created_time = models.DateTimeField(verbose_name=_("Created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("Modified time"), auto_now=True)

    class Meta:
        verbose_name = _("Activity")
        verbose_name_plural = _("Activities")
        unique_together = (('priority', 'mission'),)
        ordering = ['priority']

    def __str__(self):
        return self.title
