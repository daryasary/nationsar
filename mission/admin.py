from django.contrib import admin
from django.contrib.admin import register

from mission.models import Activity, Mission


@register(Mission)
class MissionAdmin(admin.ModelAdmin):
    list_display = ('title', 'featured', 'priority')


@register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ("title", "featured", "priority", "mission")

