from modeltranslation.translator import translator, TranslationOptions

from mission.models import Mission


class MissionTranslationOptions(TranslationOptions):
    fields = ('title', 'body')


translator.register(Mission, MissionTranslationOptions)
