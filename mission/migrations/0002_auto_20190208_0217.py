# Generated by Django 2.1.1 on 2019-02-07 22:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mission', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='feature_image',
            field=models.ImageField(upload_to='activity/feature', verbose_name='Feature image'),
        ),
        migrations.AlterField(
            model_name='activity',
            name='header_image',
            field=models.ImageField(upload_to='activity/headers', verbose_name='Header image'),
        ),
    ]
